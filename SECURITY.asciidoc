////
  // Copyright (c) 2021, 2024 Contributors to the Eclipse Foundation
  //
  // This program and the accompanying materials are made
  // available under the terms of the Eclipse Public License 2.0
  // which is available at https://www.eclipse.org/legal/epl-2.0/
  //
  // SPDX-License-Identifier: EPL-2.0
////

include::_initCommon.adoc[]

# Security Policy

The {lsat} project follows the https://www.eclipse.org/security/policy.php[Eclipse Vulnerability Reporting Policy].
Vulnerabilities are tracked by the Eclipse security team, in cooperation with the LSAT project leads.
Fixing vulnerabilities is taken care of by the LSAT project committers, with assistance and guidance of the security team. 

## Supported Versions

Whenever the {lsat} project fixes a vulnerability, it will be included in the next release.

## Reporting a Vulnerability

We recommend that in case of suspected vulnerabilities you do not use the LSAT public issue tracker, but instead contact the Eclipse Security Team directly via security@eclipse.org.
